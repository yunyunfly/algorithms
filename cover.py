# maximal coverage
from random import sample, randint
import time

def marginal_gain(A, d):
	lenbefore = len(A)
	lenafter = len(A | d)
	return lenafter - lenbefore

def greedy_cover(id_vect_tups, k):
	covering = set()
	used = set()
	while len(used) < k and len(id_vect_tups) > 0:
		# generate tups of form (id, marginal_gain)
		
		l = map(lambda x: (x[0], marginal_gain(covering, x[1]), x[1]), id_vect_tups)
		
		l = sorted(l, key=lambda x: x[1], reverse=True)
		
		#print 'l:', l
		#print 'best:', l[0][0]
		
		used.add(l[0][0])
		#print 'adding', l[0][2]
		covering.update(l[0][2])
		id_vect_tups = [t for t in id_vect_tups if t[0] not in used]
		#print 'id_vect_tups:', id_vect_tups
		#print 'used:', used
	return covering, used

def lazy_greedy_cover():
	pass


def test():
	data = [(1,set([1,2,3])), (2, set([2,4])), (3, set([4,5,6]))]
	data = []
	for i in range(100):
		d = sample(range(100), randint(1,15))
		data.append((i, set(d)))
	
	start = time.time()
	covering, used = greedy_cover(data, 10)
	print 'used doc ids:', used
	print ''
	print 'covered \'topics\':', covering
	end = time.time()
	print end - start, 'seconds'

test()
